# XLR Killer

A simple killswitch for XLR signals such as microphones

* Active device powered from 9 to 12V DC
* Internal switch uba status LED
* Remote controllable via 6.3 mm jack which overwrites the internal switch when connected
* Compatible to phantom powered systems
